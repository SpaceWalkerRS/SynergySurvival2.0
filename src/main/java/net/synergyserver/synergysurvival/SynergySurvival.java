package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergysurvival.commands.DeathsCheckCommand;
import net.synergyserver.synergysurvival.commands.DeathsCommand;
import net.synergyserver.synergysurvival.commands.DeathsTopCommand;
import net.synergyserver.synergysurvival.listeners.EntityListener;
import net.synergyserver.synergysurvival.listeners.PlayerListener;
import net.synergyserver.synergysurvival.listeners.SynergyListener;
import net.synergyserver.synergysurvival.listeners.WorldListener;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mongodb.morphia.Datastore;

import java.util.HashSet;
import java.util.List;

/**
 * The main class for SynergySurvival.
 */
public class SynergySurvival extends JavaPlugin implements SynergyPlugin {

    @Override
    public void onEnable() {
        getLogger().info("Hooking into SynergyCore...");
        SynergyCore.getPlugin().registerSynergyPlugin(this);

        getLogger().info("Starting the sun damage timer...");
        long sunDamageTimerInterval = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND,
                TimeUtil.TimeUnit.GAME_TICK, PluginConfig.getConfig(this).getLong("sun_damage.check_interval_seconds"));
        Bukkit.getScheduler().runTaskTimer(this, SunDamageTimer.sunDamageTimer, 0L, sunDamageTimerInterval);
    }

    public void postLoad() {
        // If should convert data for reset do so
        if (PluginConfig.getConfig(this).getBoolean("convert_data_for_reset")) {
            getLogger().info("Converting data for reset...");
            DataManager dm = DataManager.getInstance();
            Datastore ds = MongoDB.getInstance().getDatastore();

            List<WorldGroupProfile> wgps = ds.find(WorldGroupProfile.class).field("n").equalIgnoreCase(getWorldGroupName())
                    .retrievedFields(true, "pid", "pt", "at").asList();
            for (WorldGroupProfile wgp : wgps) {
                MinecraftProfile mcp = dm.getPartialDataEntity(MinecraftProfile.class, wgp.getPlayerID(), "lw", "wp");

                if (mcp.getLastWorldGroup() != null && mcp.getLastWorldGroup().equalsIgnoreCase(getWorldGroupName())) {
                    mcp.setLastWorldGroup("*");
                }

                WorldGroupProfile defaultWGP = mcp.getPartialWorldGroupProfile("*", "pt", "at");

                if (defaultWGP != null) {
                    defaultWGP.setPlaytime(defaultWGP.getPlaytime() + wgp.getPlaytime());
                    defaultWGP.setAfktime(defaultWGP.getAfktime() + wgp.getAfktime());
                }

                mcp.getWorldGroupProfileIDs().remove(wgp.getID());
                mcp.setWorldGroupProfileIDs(mcp.getWorldGroupProfileIDs());
                dm.deleteDataEntity(wgp);
                getLogger().info("Deleted Survival WGP of " + PlayerUtil.getName(wgp.getPlayerID()));
            }

            List<MinecraftProfile> mcps = ds.find(MinecraftProfile.class).retrievedFields(true, "wp", "n").asList();
            for (MinecraftProfile mcp : mcps) {
                HashSet<ObjectId> wgpIDs = mcp.getWorldGroupProfileIDs();
                HashSet<ObjectId> newWGPIDs = new HashSet<>();

                for (ObjectId wgp : wgpIDs) {
                    if (dm.isDataEntityInDatabase(WorldGroupProfile.class, wgp)) {
                        newWGPIDs.add(wgp);
                    } else {
                        getLogger().info("Removed linked Survival WGP with ID " + wgp.toString() + " of " + mcp.getCurrentName());
                    }
                }

                mcp.setWorldGroupProfileIDs(newWGPIDs);
            }

            // Only convert once
            PluginConfig.getConfig(this).set("convert_data_for_reset", false);
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving plugin configuration...");
        PluginConfig.getInstance().save(this);

        getLogger().info("Saving messages...");
        Message.getInstance().save(this);
    }

    public void registerCommands() {
        CommandManager cm = CommandManager.getInstance();

        cm.registerMainCommand(this, DeathsCommand.class);
        cm.registerSubCommand(this, DeathsCheckCommand.class);
        cm.registerSubCommand(this, DeathsTopCommand.class);
    }

    public void registerSettings() {
        SettingManager sm = SettingManager.getInstance();
        sm.registerSettings(SurvivalTieredMultiOptionSetting.class);
    }

    public void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new SynergyListener(), this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new EntityListener(), this);
        pm.registerEvents(new WorldListener(), this);
        // pm.registerEvents(new ItemTracker(), this);
    }

    public void mapClasses() {
        MongoDB db = MongoDB.getInstance();
        db.mapClasses(
                SurvivalWorldGroupProfile.class
        );
    }

    /**
     * Convenience method for getting this plugin.
     *
     * @return The object representing this plugin.
     */
    public static SynergySurvival getPlugin() {
        return (SynergySurvival) getProvidingPlugin(SynergySurvival.class);
    }

    public String[] getAliases() {
        return new String[]{getName(), "SynSurvival", "Survival", "SynSU", "SU"};
    }

    /**
     * Gets the list of worlds that this plugin handles.
     *
     * @return The world group of this plugin.
     */
    public static List<String> getWorldGroup() {
        return PluginConfig.getConfig(getPlugin()).getStringList("world_group.worlds");
    }

    /**
     * Gets the name of the world group that this plugin handles.
     *
     * @return The world group name of this plugin.
     */
    public static String getWorldGroupName() {
        return PluginConfig.getConfig(getPlugin()).getString("world_group.name");
    }
}