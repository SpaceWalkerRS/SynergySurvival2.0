package net.synergyserver.synergysurvival.projectiles;

import net.minecraft.server.v1_14_R1.Entity;
import net.minecraft.server.v1_14_R1.EntityLargeFireball;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.lang.reflect.Field;
import java.util.UUID;

/**
 * Represents a fireball that targets the nearest player.
 */
public class SeekingFireballBase extends EntityLargeFireball {

    private UUID tracking;
    private boolean seeking;

    public SeekingFireballBase(Location location, LivingEntity shooter) {
        super(((CraftWorld) location.getWorld()).getHandle(), ((CraftLivingEntity) shooter).getHandle(),
                location.getX(), location.getY(), location.getZ());
        this.seeking = true;
    }

    /**
     * Gets the Bukkit Location object of this fireball.
     *
     * @return The Location of this fireball.
     */
    public Location getLocation() {
        return new Location(world.getWorld(), locX, locY, locZ, yaw, pitch);
    }

    @Override
    public void tick() {
        super.tick();

        // If seeking is set to false then don't try to track a player
        if (!seeking) {
            return;
        }

        // Update the player that is being tracked every 10 ticks
        if (ticksLived % 10 == 0 || tracking == null) {
            // Find the nearest player and track them
            Player nearestPlayer = null;
            double nearestPlayerDistance = Double.MAX_VALUE;

            for (Player p : Bukkit.getOnlinePlayers()) {
                // Ignore this player if they aren't in the same world
                if (!p.getWorld().equals(world.getWorld())) {
                    continue;
                }

                // Ignore this player if they have a weak difficulty
                if (!SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile(p), "extremely_easy")) {
                    continue;
                }

                double distance = getLocation().distanceSquared(p.getLocation());

                if (distance < nearestPlayerDistance) {
                    nearestPlayer = p;
                    nearestPlayerDistance = distance;
                }
            }

            // If the nearest player is too far away then just fly straight
            if (nearestPlayerDistance > 40000) {
                return;
            }

            // Otherwise update the tracked player
            tracking = nearestPlayer.getUniqueId();
        }

        // Check that the player being tracked is still online before continuing
        Player trackedPlayer = Bukkit.getPlayer(this.tracking);

        if (trackedPlayer == null) {
            return;
        }

        // Calculate which way the fireball needs to face to target the player
        Vector currentVector = new Vector(locX, locY, locZ);
        Vector playerVector = trackedPlayer.getLocation().toVector();
        Vector direction = playerVector.subtract(currentVector);

        // Prepare the vector to be used as a direction and soften it
        direction.normalize().multiply(0.1);

        // Change the acceleration
        dirX = direction.getX();
        dirY = direction.getY();
        dirZ = direction.getZ();
    }

    /**
     * Sets the Bukkit entity of this projectile.
     *
     * @param entity The Bukkit entity of this projectile.
     */
    public void setBukkitEntity(CraftEntity entity) {
        try {
            Field f = Entity.class.getDeclaredField("bukkitEntity");
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            f.set(this, entity);
        } catch (NoSuchFieldException|IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks whether this fireball is currently seeking players.
     *
     * @return True if it's trying to target players.
     */
    public boolean isSeeking() {
        return seeking;
    }

    /**
     * Sets whether this fireball is seeking players.
     *
     * @param seeking True to try to target players.
     */
    public void setSeeking(boolean seeking) {
        this.seeking = seeking;
    }
}
