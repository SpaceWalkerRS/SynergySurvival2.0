package net.synergyserver.synergysurvival.listeners;

import com.onarandombox.MultiverseCore.MultiverseCore;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergysurvival.ItemDataManager;
import net.synergyserver.synergysurvival.SurvivalWorldGroupProfile;
import net.synergyserver.synergysurvival.SynergySurvival;
import net.synergyserver.synergysurvival.projectiles.BrickProjectile;
import net.synergyserver.synergysurvival.projectiles.FlintProjectile;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import static org.bukkit.Material.PLAYER_HEAD;

/**
 * Listens to all player-related events that are not covered by other listeners.
 */
public class PlayerListener implements Listener {

    private ItemDataManager idm = ItemDataManager.getInstance();

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerEnterWorldGroup(PlayerTeleportEvent event) {
        String toWorldGroup = SynergyCore.getWorldGroupName(event.getTo().getWorld().getName());

        // Ignore this event if the player isn't attempting to teleport to this world group
        if (!SynergySurvival.getWorldGroupName().equals(toWorldGroup)) {
            return;
        }

        Player player = event.getPlayer();
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore this event if the player is joining the world group for the first time
        if (!mcp.hasWorldGroupProfile(toWorldGroup)) {
            return;
        }

        WorldGroupProfile worldGroupProfile = mcp.getWorldGroupProfile(toWorldGroup);

        // Ignore this event if the WGP isn't a survival WGP for whatever reason
        if (!(worldGroupProfile instanceof SurvivalWorldGroupProfile)) {
            return;
        }

        SurvivalWorldGroupProfile wgp = (SurvivalWorldGroupProfile) worldGroupProfile;

        // If the player has died recently and their difficulty is high enough, prevent them from entering the world group
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(wgp.getSettingPreferences().getCurrentSettings(),
                player, "really_easy")) {
            long tempbanLength = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.MILLI,
                    PluginConfig.getConfig(SynergySurvival.getPlugin()).getLong("death_temp_ban_minutes"));
            long millisPassed = System.currentTimeMillis() - wgp.getLastDeath();
            long remainingTime = tempbanLength - millisPassed;
            if (remainingTime > 0) {
                event.setCancelled(true);

                String timeMessage = Message.getTimeMessage(remainingTime, 2, 1,
                        Message.get("info_colored_lists.item_color_1"),
                        Message.getColor("c7")
                );
                player.sendMessage(Message.format("events.teleport.error.recent_death", timeMessage));
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerSleep(PlayerBedEnterEvent event) {
        World world = event.getPlayer().getWorld();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(world.getName())) {
            return;
        }

        // Count the number of players sleeping
        // Count the event player manually since they're not technically sleeping yet
        int playersInWorld = 1;
        int sleepingPlayers = 1;
        int afkPlayers = 0;
        for (Player p : Bukkit.getOnlinePlayers()) {
            // Ignore players not in the same world
            if (!p.getWorld().equals(world)) {
                continue;
            }

            // Ignore the event player since they were already counted
            if (p.equals(event.getPlayer())) {
                continue;
            }

            // Increment the counter for players in the world
            playersInWorld++;

            // Ignore AFK players
            if (PlayerUtil.getProfile(p).isAFK()) {
                afkPlayers++;
                continue;
            }

            // Increment the counter if they're sleeping
            if (p.isSleeping()) {
                sleepingPlayers++;
            }
        }

        // If the majority of non-AFK people are sleeping, set the time to day
        if (sleepingPlayers > ((playersInWorld - afkPlayers) / 2.0F)) {
            world.setTime(0);
            world.setStorm(false);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent event) {
        Entity entity = event.getRightClicked();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        if (event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.MILK_BUCKET)) {
            boolean babied = false;
            if (entity instanceof Ageable) {
                Ageable ageable = (Ageable) entity;
                if (ageable.isAdult()) {
                    ageable.setBaby();
                    babied = true;
                }
            } else if (entity instanceof Zombie) {
                Zombie zombie = (Zombie) entity;
                if (!zombie.isBaby()) {
                    zombie.setBaby(true);
                    babied = true;
                }
            } else if (entity instanceof Slime) {
                Slime slime = (Slime) entity;
                if (slime.getSize() > 1) {
                    slime.setSize(1);
                    babied = true;
                }
            }

            if (babied) {
                event.getPlayer().getInventory().getItemInMainHand().setType(Material.BUCKET);
                event.getPlayer().updateInventory();
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerRightClick(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        World world = player.getWorld();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(world.getName())) {
            return;
        }

        // Ignore the event if they're not right clicking
        if (!(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
            return;
        }

        // Item handling
        switch (event.getMaterial()) {
            // Flint are rapid-fire projectiles that do small damage
            case FLINT:
                // Remove 1 item from their inventory
                if (event.getHand().equals(EquipmentSlot.HAND)) {
                    if(player.getInventory().getItemInMainHand().getAmount() > 1){
                        player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
                    }
                    else{
                        player.getInventory().setItemInMainHand(null);
                    }
                }
                else if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
                    if(player.getInventory().getItemInOffHand().getAmount() > 1){
                        player.getInventory().getItemInOffHand().setAmount(player.getInventory().getItemInOffHand().getAmount() - 1);
                    }
                    else{
                        player.getInventory().setItemInOffHand(null);
                    }
                }
                // Subtract 1 from circulation
                //idm.subtract(Material.FLINT, 0, 1);

                FlintProjectile flint = new FlintProjectile(player.getLocation().add(0, 1, 0), player);

                // Don't use the item in their hand for other purposes
                event.setUseInteractedBlock(Event.Result.DENY);
                event.setUseItemInHand(Event.Result.DENY);
                break;
            // Bricks are heavy, lobbed projectiles that do large damage and have potion effects
            case BRICK:
                // Remove 1 item from their inventory
                if (event.getHand().equals(EquipmentSlot.HAND)) {
                    if(player.getInventory().getItemInMainHand().getAmount() > 1){
                        player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
                    }
                    else{
                        player.getInventory().setItemInMainHand(null);
                    }
                }
                else if (event.getHand().equals(EquipmentSlot.OFF_HAND)) {
                    if(player.getInventory().getItemInOffHand().getAmount() > 1){
                        player.getInventory().getItemInOffHand().setAmount(player.getInventory().getItemInOffHand().getAmount() - 1);
                    }
                    else{
                        player.getInventory().setItemInOffHand(null);
                    }
                }
                // Subtract 1 from circulation
                //idm.subtract(Material.BRICK, 0, 1);

                BrickProjectile brick = new BrickProjectile(player.getLocation().add(0, 1, 0), player);

                // Don't use the item in their hand for other purposes
                event.setUseInteractedBlock(Event.Result.DENY);
                event.setUseItemInHand(Event.Result.DENY);
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a survival WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof SurvivalWorldGroupProfile)) {
            return;
        }

        // Increment this player's death counter
        SurvivalWorldGroupProfile wgp = (SurvivalWorldGroupProfile) mcp.getCurrentWorldGroupProfile();
        wgp.setDeaths(wgp.getDeaths() + 1);

        // Set the player's last death time
        wgp.setLastDeath(System.currentTimeMillis());

        // Reset the damage timer that prevents teleports
        wgp.setLastDamage(0);

        // If they got killed by a player then have a 1/20 chance of dropping their skull
        if (player.getKiller() != null && MathUtil.randChance(.05)) {
            ItemStack skull = new ItemStack(PLAYER_HEAD, 1, (short) 3);
            SkullMeta meta = ((SkullMeta) skull.getItemMeta());

            meta.setOwner(player.getName());
            skull.setItemMeta(meta);
            player.getWorld().dropItem(player.getLocation(),skull);
        }

        // If the player's difficulty is high enough, discard their inventory
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "somewhat_easy")) {
            event.getDrops().clear();
        }

        // If the player's difficulty is high enough, randomly spawn a zombie or skeleton
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "extremely_easy")) {
            if (MathUtil.randChance(0.5)) {
                player.getWorld().spawnEntity(player.getLocation(), EntityType.ZOMBIE);
            } else {
                player.getWorld().spawnEntity(player.getLocation(), EntityType.SKELETON);
            }
        }


        // If the player will be tempbanned as a result of the death, give them a message
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "really_easy")) {
            long tempbanLength = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.MINUTE, TimeUtil.TimeUnit.MILLI,
                    PluginConfig.getConfig(SynergySurvival.getPlugin()).getLong("death_temp_ban_minutes"));
            String timeMessage = Message.getTimeMessage(tempbanLength, 2, 1,
                    Message.get("info_colored_lists.item_color_1"),
                    Message.get("info_colored_lists.grammar_color")
            );
            player.sendMessage(Message.format("events.death.info.tempbanned", timeMessage));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDamageByEntity(EntityDamageByEntityEvent event){
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't a player that got damaged
        if (!(entity instanceof Player)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile((Player) entity);

        switch (event.getCause()) {
            case ENTITY_EXPLOSION:
            case BLOCK_EXPLOSION:
                // Multiply explosion damage by 1.2 if the player has a high enough difficulty
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "extremely_easy")) {
                    // If its explosion was changed by this plugin, adjust the damage for that so it's not an insta-kill
                    double baseDamage = event.getDamage();
                    Entity damager = event.getDamager();

                    if (damager.hasMetadata("explosionRadius")) {
                        baseDamage = baseDamage / (damager.getMetadata("explosionRadius").get(0).asFloat() / 2);
                    }

                    event.setDamage(baseDamage * 1.2);
                }
                break;
            case DRAGON_BREATH:
            case ENTITY_ATTACK:
            case ENTITY_SWEEP_ATTACK:
            case PROJECTILE:
                // Multiply the damage by 1.5 if it's from a mob and the player has a high enough difficulty
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "somewhat_easy")) {
                    event.setDamage(event.getDamage() * 1.5);
                }
                break;
            default: break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't a player that got damaged
        if (!(entity instanceof Player)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile((Player) entity);

        // Ignore the event if, for whatever reason, the current WGP is not a survival WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof SurvivalWorldGroupProfile)) {
            return;
        }

        // Set the last time the player took damage
        SurvivalWorldGroupProfile wgp = (SurvivalWorldGroupProfile) mcp.getCurrentWorldGroupProfile();
        wgp.setLastDamage(System.currentTimeMillis());
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't caused by a plugin or command
        if (!(event.getCause().equals(PlayerTeleportEvent.TeleportCause.PLUGIN) || event.getCause().equals(PlayerTeleportEvent.TeleportCause.COMMAND))){
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Ignore the event if, for whatever reason, the current WGP is not a survival WGP
        if (!(mcp.getCurrentWorldGroupProfile() instanceof SurvivalWorldGroupProfile)) {
            return;
        }

        SurvivalWorldGroupProfile wgp = (SurvivalWorldGroupProfile) mcp.getCurrentWorldGroupProfile();

        // If they took damage in the last 3 seconds then prevent them from teleporting
        if ((System.currentTimeMillis() - wgp.getLastDamage()) < 3000) {
            String message = Message.getTimeMessage(3000 - (System.currentTimeMillis() - wgp.getLastDamage()), 2, 1);
            player.sendMessage(Message.format("events.teleport.error.recent_damage", message));
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerEat(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }


        // Only care if the target is a player with a higher difficulty
        if (!SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(PlayerUtil.getProfile(player), "extremely_easy")) {
            return;
        }

        // Give them food poisoning if they eat raw meat
        switch (event.getItem().getType()) {
            case BEEF:
            case CHICKEN:
            case MUTTON:
            case RABBIT:
            case PORKCHOP:
            case ROTTEN_FLESH:
                player.addPotionEffect(new PotionEffect(PotionEffectType.POISON,100,0));
                player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER,200,0));
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onAnvilUse(PrepareAnvilEvent event) {
        AnvilInventory inventory = event.getInventory();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(inventory.getLocation().getWorld().getName())) {
            return;
        }

        // Disable until Craftbukkit stops being stupid

        /*if (inventory.getRepairCost() > 40) {
            inventory.setRepairCost(39);
        }*/
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerHeal(EntityRegainHealthEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore the event if the entity iss not a player
        if (!(entity instanceof Player)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile((Player) entity);

        switch (event.getRegainReason()) {
            case REGEN:
                // If it's natural regeneration, cancel it if their difficulty is high enough
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "very_easy")) {
                    event.setCancelled(true);
                }
                break;
            case MAGIC:
            case MAGIC_REGEN:
                // If it's potion/beacon regeneration, cancel it if their difficulty is high enough
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "quite_easy")) {
                    event.setCancelled(true);
                }
                break;
            default:
                // Cancel regeneration if their difficulty is high enough
                if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "easy")) {
                    event.setCancelled(true);
                }
                break;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerSpawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergySurvival.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Set their health to half a heart if their difficulty is high enough
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "easy")) {
            Bukkit.getScheduler().runTaskLater(SynergySurvival.getPlugin(), () -> {
                player.setHealth(1D);
            }, 0);
        }

        // If the player's difficulty is high enough, kick them out of survival
        if (SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "really_easy")) {
            MultiverseCore multiverse = (MultiverseCore) Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            Bukkit.getScheduler().runTaskLater(SynergySurvival.getPlugin(), () -> {
                player.teleport(multiverse.getMVWorldManager().getFirstSpawnWorld().getSpawnLocation());
            }, 0);
        }
    }
}
